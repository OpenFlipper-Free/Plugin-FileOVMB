/*===========================================================================*\
*                                                                            *
 *                              OpenFlipper                                   *
 *           Copyright (c) 2001-2015, RWTH-Aachen University                 *
 *           Department of Computer Graphics and Multimedia                  *
 *                          All rights reserved.                             *
 *                            www.openflipper.org                            *
 *                                                                           *
 *---------------------------------------------------------------------------*
 * This file is part of OpenFlipper.                                         *
 *---------------------------------------------------------------------------*
 *                                                                           *
 * Redistribution and use in source and binary forms, with or without        *
 * modification, are permitted provided that the following conditions        *
 * are met:                                                                  *
 *                                                                           *
 * 1. Redistributions of source code must retain the above copyright notice, *
 *    this list of conditions and the following disclaimer.                  *
 *                                                                           *
 * 2. Redistributions in binary form must reproduce the above copyright      *
 *    notice, this list of conditions and the following disclaimer in the    *
 *    documentation and/or other materials provided with the distribution.   *
 *                                                                           *
 * 3. Neither the name of the copyright holder nor the names of its          *
 *    contributors may be used to endorse or promote products derived from   *
 *    this software without specific prior written permission.               *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED *
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A           *
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER *
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,  *
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,       *
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR        *
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF    *
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING      *
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS        *
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.              *
 *                                                                            *
 \*===========================================================================*/

#pragma once

#include <QObject>
#include <QMenuBar>
#include <QComboBox>
#include <QCheckBox>

#include <OpenFlipper/common/Types.hh>
#include <OpenFlipper/BasePlugin/BaseInterface.hh>
#include <OpenFlipper/BasePlugin/FileInterface.hh>
#include <OpenFlipper/BasePlugin/LoadSaveInterface.hh>
#include <OpenFlipper/BasePlugin/LoggingInterface.hh>
#include <OpenFlipper/BasePlugin/ScriptInterface.hh>

#include <ObjectTypes/PolyhedralMesh/PolyhedralMesh.hh>
#include <ObjectTypes/HexahedralMesh/HexahedralMesh.hh>
#include <ObjectTypes/TetrahedralMesh/TetrahedralMesh.hh>
#include <OpenVolumeMesh/IO/ovmb_read.hh>
#include <OpenVolumeMesh/IO/ovmb_write.hh>

class FileOVMBPlugin: public QObject,
        BaseInterface,
        FileInterface,
        LoadSaveInterface,
        LoggingInterface
{
Q_OBJECT
Q_INTERFACES(BaseInterface)
Q_INTERFACES(FileInterface)
Q_INTERFACES(LoadSaveInterface)
Q_INTERFACES(LoggingInterface)
Q_PLUGIN_METADATA(IID "org.OpenFlipper.Plugins.Plugin-FileOVMB")
public:
    FileOVMBPlugin() = default;
    ~FileOVMBPlugin() = default;

signals:
    // BaseInterface:
    void updatedObject(int _id, const UpdateType& _type) override;

    // FileInterface:
    void openedFile(int _id) override;
    void load(QString _filename, DataType _type, int& _id) override;
    void save(int _id, QString _filename) override;

    // LoadSaveInterface:
    void addEmptyObject(DataType _type, int& _id) override;

    // LoggingInterface:
    void log(Logtype _type, QString _message) override;
    void log(QString _message) override;


private slots:

    // BaseInterface:
    void initializePlugin() override;
    void noguiSupported() override {}

public:

    // BaseInterface:
    QString name() override;
    QString description() override;
    QString version() override;

    // FileInterface
    DataType supportedType() override;
    QString getSaveFilters() override;
    QString getLoadFilters() override;
    QWidget *saveOptionsWidget(QString) override;;
    QWidget *loadOptionsWidget(QString) override;;

public slots:
    // FileInterface:
    int loadObject(QString _filename) override;
    bool saveObject(int _id, QString _filename) override;

private:
    QWidget* loadOptions_ = nullptr;
    QCheckBox* loadTopCheck_ = nullptr;
};

