/*===========================================================================*\
*                                                                            *
 *                              OpenFlipper                                   *
 *           Copyright (c) 2001-2015, RWTH-Aachen University                 *
 *           Department of Computer Graphics and Multimedia                  *
 *                          All rights reserved.                             *
 *                            www.openflipper.org                            *
 *                                                                           *
 *---------------------------------------------------------------------------*
 * This file is part of OpenFlipper.                                         *
 *---------------------------------------------------------------------------*
 *                                                                           *
 * Redistribution and use in source and binary forms, with or without        *
 * modification, are permitted provided that the following conditions        *
 * are met:                                                                  *
 *                                                                           *
 * 1. Redistributions of source code must retain the above copyright notice, *
 *    this list of conditions and the following disclaimer.                  *
 *                                                                           *
 * 2. Redistributions in binary form must reproduce the above copyright      *
 *    notice, this list of conditions and the following disclaimer in the    *
 *    documentation and/or other materials provided with the distribution.   *
 *                                                                           *
 * 3. Neither the name of the copyright holder nor the names of its          *
 *    contributors may be used to endorse or promote products derived from   *
 *    this software without specific prior written permission.               *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED *
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A           *
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER *
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,  *
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,       *
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR        *
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF    *
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING      *
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS        *
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.              *
 *                                                                            *
 \*===========================================================================*/


#include <iostream>

#include <OpenFlipper/BasePlugin/PluginFunctions.hh>
#include <OpenFlipper/common/GlobalOptions.hh>
#include <ACG/Math/VectorT.hh>
#include <ACG/Math/QuaternionT.hh>
#include <ACG/Math/Matrix3x3T.hh>
#include <ACG/Math/Matrix4x4T.hh>
#include <OpenVolumeMesh/IO/PropertyCodecsT_impl.hh>


#include "FileOVMB.hh"
#include "PluginFunctionsOVMB.hh"

#include <QVBoxLayout>

void FileOVMBPlugin::initializePlugin()
{
    auto &pc = ovmb_property_codecs();
    pc.add_default_types();
    pc.register_arraylike<ACG::VectorT<double, 2>>("2d");
    pc.register_arraylike<ACG::VectorT<double, 3>>("3d");
    pc.register_arraylike<ACG::VectorT<double, 4>>("4d");

    pc.register_arraylike<ACG::VectorT<float, 2>>("2f");
    pc.register_arraylike<ACG::VectorT<float, 3>>("3f");
    pc.register_arraylike<ACG::VectorT<float, 4>>("4f");

    pc.register_arraylike<ACG::QuaternionT<double>>("quat_d");
    pc.register_arraylike<ACG::QuaternionT<float>>("quat_f");

    pc.register_matrixlike<ACG::Matrix3x3T<double>, 3, 3>("3x3d");
    pc.register_matrixlike<ACG::Matrix3x3T<float>, 3, 3>("3x3f");

    pc.register_matrixlike<ACG::Matrix4x4T<double>, 4, 4>("4x4d");
    pc.register_matrixlike<ACG::Matrix4x4T<float>, 4, 4>("4x4f");

    if (!OpenFlipper::Options::nogui())
    {
        loadOptions_ = new QWidget();
        QVBoxLayout* llayout = new QVBoxLayout();
        llayout->setAlignment(Qt::AlignTop);
        loadTopCheck_ = new QCheckBox("Perform topology checks");
        llayout->addWidget(loadTopCheck_);
        loadOptions_->setLayout(llayout);
    }
}

QString FileOVMBPlugin::name()
{
    return "FileOVMB";
}

QString FileOVMBPlugin::description()
{
    return tr("Load/Save OpenVolumeMesh .ovmb format");
}

QString FileOVMBPlugin::version()
{
    return "1.0";
}

DataType FileOVMBPlugin::supportedType()
{
    return DATA_POLYHEDRAL_MESH | DATA_HEXAHEDRAL_MESH | DATA_TETRAHEDRAL_MESH;
}

QString FileOVMBPlugin::getSaveFilters()
{
    return tr("OpenVolumeMesh files ( *.ovmb )");
}

QString FileOVMBPlugin::getLoadFilters()
{
    return tr("OpenVolumeMesh files ( *.ovmb )");
}

QWidget *FileOVMBPlugin::saveOptionsWidget(QString)
{
    return nullptr;
}

QWidget *FileOVMBPlugin::loadOptionsWidget(QString)
{
    return loadOptions_;
}

int FileOVMBPlugin::loadObject(QString _filename)
{
    OpenVolumeMesh::IO::ReadOptions options;
    options.topology_check = true;

    if(!OpenFlipper::Options::nogui()) {
        options.topology_check = loadTopCheck_->isChecked();
    }

    int id = -1;

    std::ifstream f(_filename.toStdString(), std::ios::binary);
    if (!f.good()) {
        emit log(LOGERR, QString("Could not open file %1!").arg(_filename));
        return -1;
    }

    auto reader = make_ovmb_reader(f, options, ovmb_property_codecs());
    auto topo_type = reader->topo_type();
    if (!topo_type.has_value()) {
        emit log(LOGERR, QString("Could not read file %1!").arg(_filename));
        return -1;
    }
    using TopoType = OpenVolumeMesh::IO::detail::TopoType;
    using ReadResult = OpenVolumeMesh::IO::ReadResult;
    ReadResult result;


    BaseObjectData* obj = nullptr;
    if (*topo_type == TopoType::Tetrahedral) {
        TetrahedralMesh mesh;
        result = reader->read_file(mesh);
        if (result == ReadResult::Ok) {
            emit addEmptyObject(DATA_TETRAHEDRAL_MESH, id);
            auto *mo = PluginFunctions::tetrahedralMeshObject(id);
            obj = mo;
            *mo->mesh() = mesh; // TODO: use move assignment when we have it implemented in OVM
            mo->recreateAttributes();
        }
    } else if (*topo_type == TopoType::Hexahedral) {
        HexahedralMesh mesh;
        result = reader->read_file(mesh);
        if (result == ReadResult::Ok) {
            emit addEmptyObject(DATA_HEXAHEDRAL_MESH, id);
            auto *mo = PluginFunctions::hexahedralMeshObject(id);
            obj = mo;
            *mo->mesh() = mesh; // TODO: use move assignment when we have it implemented in OVM
            mo->recreateAttributes();
        }
    } else if (*topo_type == TopoType::Polyhedral) {
        PolyhedralMesh mesh;
        result = reader->read_file(mesh);
        if (result == ReadResult::Ok) {
            emit addEmptyObject(DATA_POLYHEDRAL_MESH, id);
            auto *mo = PluginFunctions::polyhedralMeshObject(id);
            obj = mo;
            *mo->mesh() = mesh; // TODO: use move assignment when we have it implemented in OVM
            mo->recreateAttributes();
        }
    } else {
        emit log(LOGERR, QString("Unknown topology in file %1!").arg(_filename));
        return -1;
    }
    if (result != ReadResult::Ok) {
        emit log(LOGERR, QString("Failed to load OVMB file:") + to_string(result));
        return -1;
    }
    obj->setFromFileName(_filename);
    obj->setName(obj->filename());

    obj->setObjectDrawMode(ACG::SceneGraph::DrawModes::getDrawMode("Cells (flat shaded)"));

    emit updatedObject(obj->id(), UPDATE_ALL);
    emit openedFile(obj->id());

    return id;
}


bool FileOVMBPlugin::saveObject(int _id, QString _filename)
{
    BaseObjectData* obj = nullptr;
    if (!PluginFunctions::getObject(_id, obj)) {
        emit log(LOGERR, tr("saveObject : cannot get object id %1 for save name %2").arg(_id).arg(_filename) );
        return false;
    }

    obj->setFromFileName(_filename);
    obj->setName(obj->filename());

    using WriteOptions = OpenVolumeMesh::IO::WriteOptions;
    using TopoType = WriteOptions::TopologyType;

    WriteOptions options;

    std::string filename = _filename.toStdString();

    OpenVolumeMesh::IO::WriteResult result;

    if (auto *mo = PluginFunctions::polyhedralMeshObject(obj)) {
        options.topology_type = TopoType::Polyhedral;
        result = ovmb_write(filename.c_str(), *mo->mesh(), options, ovmb_property_codecs());
    } else if (auto *mo = PluginFunctions::tetrahedralMeshObject(obj)) {
        options.topology_type = TopoType::Tetrahedral;
        result = ovmb_write(filename.c_str(), *mo->mesh(), options, ovmb_property_codecs());
    } else if (auto *mo = PluginFunctions::hexahedralMeshObject(obj)) {
        options.topology_type = TopoType::Hexahedral;
        result = ovmb_write(filename.c_str(), *mo->mesh(), options, ovmb_property_codecs());
    } else {
        return false;
    }
    return result == OpenVolumeMesh::IO::WriteResult::Ok;
}

