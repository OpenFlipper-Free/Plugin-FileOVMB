#pragma once
#include <OpenFlipper/common/GlobalDefines.hh>
#include <OpenVolumeMesh/IO/PropertyCodecs.hh>


DLLEXPORT OpenVolumeMesh::IO::PropertyCodecs &ovmb_property_codecs();
