#include "PluginFunctionsOVMB.hh"

OpenVolumeMesh::IO::PropertyCodecs &ovmb_property_codecs()
{
    static OpenVolumeMesh::IO::PropertyCodecs ovmb_prop_codecs;
    return ovmb_prop_codecs;
}
